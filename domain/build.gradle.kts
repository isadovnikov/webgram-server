plugins {
    kotlin("jvm")
}

group = "com.isadounikau"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
}
