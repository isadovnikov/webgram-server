package com.isadounikau.webgram.core.util

import com.isadounikau.webgram.core.domain.MarketRealTimePrice
import java.time.ZoneId
import java.time.format.DateTimeFormatter

fun MarketRealTimePrice.Included.Attributes.Value.toHourString(): String = this.datetime
    .atZoneSameInstant(ZoneId.of("Europe/Paris")).format(DateTimeFormatter.ofPattern("HH:mm"))
