package com.isadounikau.webgram.core.domain

data class ChannelId(
    val value: Long
)
