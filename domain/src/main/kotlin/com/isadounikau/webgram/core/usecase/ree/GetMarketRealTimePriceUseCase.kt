package com.isadounikau.webgram.core.usecase.ree

import com.isadounikau.webgram.core.domain.MarketRealTimePrice
import com.isadounikau.webgram.core.domain.TimeTrunc
import com.isadounikau.webgram.core.service.ReeService
import com.isadounikau.webgram.core.usecase.UseCase
import com.isadounikau.webgram.core.usecase.ree.GetMarketRealTimePriceUseCase.GetMarketRealTimePriceRequest
import com.isadounikau.webgram.core.usecase.ree.GetMarketRealTimePriceUseCase.GetMarketRealTimePriceResponse
import com.isadounikau.webgram.core.usecase.ree.GetMarketRealTimePriceUseCase.GetMarketRealTimePriceResponse.Failed.Error
import com.isadounikau.webgram.core.usecase.ree.GetMarketRealTimePriceUseCase.GetMarketRealTimePriceResponse.Success
import java.time.LocalDate

class GetMarketRealTimePriceUseCase(
    private val reeService: ReeService,
) : UseCase<GetMarketRealTimePriceRequest, GetMarketRealTimePriceResponse> {

    override fun invoke(request: GetMarketRealTimePriceRequest): GetMarketRealTimePriceResponse {
        val tomorrow = request.date
        val startDate = tomorrow.atStartOfDay()
        val endDate = startDate.plusDays(1)

        return reeService.getMarketRealTimePrice(startDate, endDate, TimeTrunc.HOUR).fold(
            onSuccess = { Success(it) },
            onFailure = { Error(it.message) }
        )
    }

    data class GetMarketRealTimePriceRequest(
        val date: LocalDate
    )

    sealed class GetMarketRealTimePriceResponse {
        data class Success(
            val price: MarketRealTimePrice
        ) : GetMarketRealTimePriceResponse()


        sealed class Failed : GetMarketRealTimePriceResponse() {
            data class Error(val message: String?) : Failed()
        }
    }
}






