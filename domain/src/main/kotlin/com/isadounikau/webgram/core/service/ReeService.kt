package com.isadounikau.webgram.core.service

import com.isadounikau.webgram.core.domain.CheapestRanges
import com.isadounikau.webgram.core.domain.MarketRealTimePrice
import com.isadounikau.webgram.core.domain.TimeTrunc
import java.io.File
import java.time.LocalDate
import java.time.LocalDateTime

interface ReeService {

    fun getMarketRealTimePrice(
        startDate: LocalDateTime,
        endDate: LocalDateTime,
        timeTrunc: TimeTrunc,
    ): Result<MarketRealTimePrice>

    fun generateElectricityDailyReport(date: LocalDate, ranges: CheapestRanges): Result<File>

}
