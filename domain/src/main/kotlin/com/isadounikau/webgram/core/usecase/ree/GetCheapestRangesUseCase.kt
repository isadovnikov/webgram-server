package com.isadounikau.webgram.core.usecase.ree

import com.isadounikau.webgram.core.domain.CheapestRange
import com.isadounikau.webgram.core.domain.CheapestRanges
import com.isadounikau.webgram.core.domain.MarketRealTimePrice
import com.isadounikau.webgram.core.domain.MarketRealTimePrice.Included.Attributes.Value
import com.isadounikau.webgram.core.domain.MarketRealTimePrice.Included.ID.PVPC
import com.isadounikau.webgram.core.usecase.UseCase
import com.isadounikau.webgram.core.usecase.ree.GetMarketRealTimePriceUseCase.GetMarketRealTimePriceResponse
import java.time.LocalDate

class GetCheapestRangesUseCase(
    private val getMarketRealTimePriceUseCase: GetMarketRealTimePriceUseCase
) : UseCase<GetCheapestRangesUseCase.GetCheapestRequest, GetCheapestRangesUseCase.GetCheapestResponse> {

    override fun invoke(request: GetCheapestRequest): GetCheapestResponse = try {
        val priceResponse = getMarketRealTimePriceUseCase(request.toGetMarketRealTimePriceRequest())
        when (priceResponse) {
            is GetMarketRealTimePriceResponse.Success -> getCheapestResponse(priceResponse.price)
            is GetMarketRealTimePriceResponse.Failed.Error -> GetCheapestResponse.Failed.Error(priceResponse.message)
        }
    } catch (ex: Exception) {
        GetCheapestResponse.Failed.Error(ex.message)
    }

    private fun getCheapestResponse(price: MarketRealTimePrice): GetCheapestResponse {
        val ranges = getLowPricesRanges(price)

        return if (ranges != null) {
            GetCheapestResponse.Success(ranges)
        } else {
            GetCheapestResponse.Failed.NotFound()
        }
    }

    private fun getLowPricesRanges(price: MarketRealTimePrice): CheapestRanges? {
        val values = price.included.firstOrNull { PVPC == it.id }
            ?.attributes
            ?.values
            ?: return null

        val sortedValues = values.sortedBy { it.value }
        val lowValues = sortedValues.take(8)

        val cheapValuesRanges = mutableListOf<List<Value>>()
        var cheapRange = mutableListOf<Value>()

        for (value in values) {
            if (lowValues.contains(value)) {
                cheapRange.add(value)
            } else if (cheapRange.size > 1) {
                cheapValuesRanges.add(cheapRange)
                cheapRange = mutableListOf()
            } else {
                cheapRange = mutableListOf()
            }
        }

        return CheapestRanges(ranges = cheapValuesRanges.map { CheapestRange(it) })
    }

    data class GetCheapestRequest(
        val date: LocalDate
    )

    sealed class GetCheapestResponse {
        data class Success(
            val data: CheapestRanges
        ) : GetCheapestResponse()

        sealed class Failed : GetCheapestResponse() {
            data class Error(val message: String?) : Failed()
            data class NotFound(
                val message: String = "Data is not found"
            ) : Failed()
        }
    }

    private fun GetCheapestRequest.toGetMarketRealTimePriceRequest() = GetMarketRealTimePriceUseCase.GetMarketRealTimePriceRequest(this.date)
}

