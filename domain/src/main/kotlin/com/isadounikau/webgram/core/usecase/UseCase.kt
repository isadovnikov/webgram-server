package com.isadounikau.webgram.core.usecase

interface UseCase<in REQUEST, out RESPONSE> {
    operator fun invoke(request: REQUEST): RESPONSE
}
