package com.isadounikau.webgram.core.util

import java.time.LocalDate
import java.time.ZoneId

fun nowInSpain() = LocalDate.now(ZoneId.of("Europe/Paris"))
