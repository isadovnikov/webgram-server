package com.isadounikau.webgram.core.usecase.api

import com.isadounikau.webgram.core.service.TelegramService
import com.isadounikau.webgram.core.usecase.UseCase
import com.isadounikau.webgram.core.usecase.api.ProcessTelegramMessageUseCase.TelegramMessageResponse
import com.isadounikau.webgram.core.usecase.api.ProcessTelegramMessageUseCase.TelegramMessageResponse.Failed.Error

open class ProcessTelegramMessageUseCase(
    private val telegramService: TelegramService,
) : UseCase<String, TelegramMessageResponse> {

    override fun invoke(request: String): TelegramMessageResponse {
        return telegramService.processUpdate(request).fold(
            onSuccess = {
                TelegramMessageResponse.Success
            },
            onFailure = {
                Error(it.message)
            },
        )
    }

    sealed class TelegramMessageResponse {
        object Success : TelegramMessageResponse()

        sealed class Failed : TelegramMessageResponse() {
            data class Error(val message: String?) : Failed()
        }
    }

}






