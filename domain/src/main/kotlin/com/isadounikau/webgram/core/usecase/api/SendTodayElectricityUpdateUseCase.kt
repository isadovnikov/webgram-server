package com.isadounikau.webgram.core.usecase.api

import com.isadounikau.webgram.core.domain.Bot
import com.isadounikau.webgram.core.service.ReeService
import com.isadounikau.webgram.core.service.TelegramService
import com.isadounikau.webgram.core.usecase.UseCase
import com.isadounikau.webgram.core.usecase.api.SendTodayElectricityUpdateUseCase.SendElectricityUpdateResponse
import com.isadounikau.webgram.core.usecase.ree.GetCheapestRangesUseCase
import com.isadounikau.webgram.core.util.nowInSpain
import java.time.LocalDate

open class SendTodayElectricityUpdateUseCase(
    private val telegramService: TelegramService,
    private val reeService: ReeService,
    private val getCheapestRangesUseCase: GetCheapestRangesUseCase,
) : UseCase<SendTodayElectricityUpdateUseCase.SendElectricityUpdateRequest, SendElectricityUpdateResponse> {

    override fun invoke(request: SendElectricityUpdateRequest): SendElectricityUpdateResponse = try {
        val date = nowInSpain().plusDays(1)
        val getCheapestRequest = GetCheapestRangesUseCase.GetCheapestRequest(date)
        val result = when (val rangesResponse = getCheapestRangesUseCase(getCheapestRequest)) {
            is GetCheapestRangesUseCase.GetCheapestResponse.Failed.Error -> {
                SendElectricityUpdateResponse.Failed.Error(rangesResponse.message)
            }
            is GetCheapestRangesUseCase.GetCheapestResponse.Failed.NotFound -> {
                SendElectricityUpdateResponse.Failed.Error(rangesResponse.message)
            }
            is GetCheapestRangesUseCase.GetCheapestResponse.Success -> sendReport(date, rangesResponse)
        }

        result
    } catch (ex: Exception) {
        SendElectricityUpdateResponse.Failed.Error(ex.message)
    }

    private fun sendReport(
        date: LocalDate,
        rangesResponse: GetCheapestRangesUseCase.GetCheapestResponse.Success
    ): SendElectricityUpdateResponse = reeService.generateElectricityDailyReport(date, rangesResponse.data).fold(
        onSuccess = {
            telegramService.sendImage(Bot.ELECTRICITY, it)
            SendElectricityUpdateResponse.Success
        },
        onFailure = { SendElectricityUpdateResponse.Failed.Error(it.message) }
    )

    object SendElectricityUpdateRequest

    sealed class SendElectricityUpdateResponse {
        object Success : SendElectricityUpdateResponse()

        sealed class Failed : SendElectricityUpdateResponse() {
            data class Error(val message: String?) : Failed()
        }
    }
}

