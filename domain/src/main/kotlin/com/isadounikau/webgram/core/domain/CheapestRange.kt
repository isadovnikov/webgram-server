package com.isadounikau.webgram.core.domain

import com.isadounikau.webgram.core.domain.MarketRealTimePrice.Included.Attributes.Value

data class CheapestRanges(
    val ranges: List<CheapestRange>
)

data class CheapestRange(
    val values: List<Value>
)
