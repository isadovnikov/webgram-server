package com.isadounikau.webgram.core.service

import com.isadounikau.webgram.core.domain.Bot
import java.io.File

interface TelegramService {

    fun processUpdate(rawUpdate: String): Result<Unit>

    fun sendMessage(bot: Bot, message: String)

    fun sendImage(bot: Bot, image: File)
}
