package com.isadounikau.webgram.core.domain

import java.time.OffsetDateTime

data class MarketRealTimePrice(
    val included: List<Included>
) {
    data class Included(
        val id: ID,
        val type: String,
        val attributes: Attributes,
    ) {
        data class Attributes(
            val values: List<Value>
        ) {
            data class Value(
                val value: Double,
                val percentage: Double,
                val datetime: OffsetDateTime,
            )
        }

        enum class ID(val id: Int) {
            PVPC(1001), SPOT(600);

            fun getCode() = this.id

            companion object {
                fun fromCode(code: Int): ID {
                    return values()
                        .firstOrNull { code == it.getCode() }
                        ?: throw EnumConstantNotPresentException(ID::class.java, code.toString())
                }
            }
        }
    }
}


enum class TimeTrunc(val apiValue: String) {
    HOUR("hour")
}
