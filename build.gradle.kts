plugins {
    kotlin("jvm") version "1.6.20"
    kotlin("plugin.allopen") version "1.6.20"
    id("io.quarkus")
}

repositories {
    mavenLocal()
    mavenCentral()
}

val quarkusPlatformGroupId: String by project
val quarkusPlatformArtifactId: String by project
val quarkusPlatformVersion: String by project

val isLinux = org.gradle.nativeplatform.platform.internal.DefaultNativePlatform.getCurrentOperatingSystem().isLinux
val skijaArtifact = if(isLinux) {
    "skija-linux"
} else {
    "skija-macos-x64"
}

dependencies {
    // Modules
    implementation(project(":domain"))

    // Quarkus
    implementation(enforcedPlatform("${quarkusPlatformGroupId}:${quarkusPlatformArtifactId}:${quarkusPlatformVersion}"))
    implementation("io.quarkus", "quarkus-hibernate-orm-panache-kotlin")
    implementation("io.quarkus", "quarkus-resteasy-reactive")
    implementation("io.quarkus", "quarkus-resteasy-reactive-jackson")
    implementation("io.quarkus", "quarkus-rest-client-reactive-jackson")
    implementation("io.quarkus", "quarkus-smallrye-openapi")
    implementation("io.quarkus", "quarkus-smallrye-metrics")
    implementation("io.quarkus", "quarkus-smallrye-health")
    implementation("io.quarkus", "quarkus-jdbc-postgresql")
    implementation("io.quarkus", "quarkus-liquibase")
    implementation("io.quarkus", "quarkus-kotlin")
    implementation("io.quarkus", "quarkus-arc")

    // Telegram
    implementation("dev.inmo", "tgbotapi.core-jvm", "0.38.12")

    api("io.github.humbleui", skijaArtifact, "0.102.0")

    // Other
    implementation("org.jetbrains.kotlin", "kotlin-stdlib-jdk8")
    implementation("io.github.microutils", "kotlin-logging", "1.12.0")

    // Test
    testImplementation("io.quarkus", "quarkus-junit5")
    testImplementation("io.quarkus", "quarkus-test-h2")
    testImplementation("io.quarkus", "quarkus-jdbc-h2")
    testImplementation("io.rest-assured", "rest-assured")
    testImplementation("org.testcontainers", "postgresql", "1.17.1")
    testImplementation("org.testcontainers", "postgresql", "1.17.1")
    testImplementation("com.github.tomakehurst", "wiremock-jre8", "2.33.1")
}

group = "com.isadounikau"
version = "1.0.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

allOpen {
    annotation("javax.ws.rs.Path")
    annotation("javax.enterprise.context.ApplicationScoped")
    annotation("javax.inject.Singleton")
    annotation("io.quarkus.test.junit.QuarkusTest")
}

tasks {
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
        kotlinOptions.javaParameters = true
    }
}
