package com.isadounikau

import com.isadounikau.webgram.infra.controller.TelegramResourceIntegrationTest
import io.quarkus.test.junit.NativeImageTest

@NativeImageTest
class NativeTelegramResourceIntegrationIT : TelegramResourceIntegrationTest()
