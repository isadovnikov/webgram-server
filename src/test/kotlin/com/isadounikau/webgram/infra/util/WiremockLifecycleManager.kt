package com.isadounikau.webgram.infra.util

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager

class WiremockLifecycleManager: QuarkusTestResourceLifecycleManager {

    private val config = WireMockConfiguration().dynamicPort()
    private lateinit var container: WireMockServer

    override fun start(): Map<String, String> {
        container = WireMockServer(config)
        container.start()

        return mapOf(
            "telegram.bot.host" to container.baseUrl(),
            "quarkus.rest-client.ree-data-api.url" to container.baseUrl(),
        )
    }

    override fun stop() {
        container.stop()
    }
}
