package com.isadounikau.webgram.infra.util

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.containers.JdbcDatabaseContainer
import org.testcontainers.containers.PostgreSQLContainerProvider

class PostgresLifecycleManager: QuarkusTestResourceLifecycleManager {

    private lateinit var container: JdbcDatabaseContainer<*>

    override fun start(): Map<String, String> {
        container = PostgreSQLContainerProvider().newInstance()
        container.start()

        return mapOf(
            "quarkus.datasource.username" to container.username,
            "quarkus.datasource.password" to container.password,
            "quarkus.datasource.jdbc.url" to container.jdbcUrl,
        )
    }

    override fun stop() {
        container.stop()
    }
}
