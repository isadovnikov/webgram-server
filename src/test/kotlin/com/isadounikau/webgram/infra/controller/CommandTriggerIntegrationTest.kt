package com.isadounikau.webgram.infra.controller

import com.isadounikau.webgram.infra.util.PostgresLifecycleManager
import com.isadounikau.webgram.infra.util.WiremockLifecycleManager
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.junit.jupiter.api.Test

@QuarkusTest
@QuarkusTestResource(PostgresLifecycleManager::class)
@QuarkusTestResource(WiremockLifecycleManager::class)
class CommandTriggerIntegrationTest {

    @Test
    fun `should trigger electricity update and return OK`() {
        // GIVEN
        val specification = given()

        // WHEN
        val request = specification.`when`().get("/api/v1/commands/electricity-update")

        // THEN
        val response = request.then()

        response.statusCode(200)
    }

}
