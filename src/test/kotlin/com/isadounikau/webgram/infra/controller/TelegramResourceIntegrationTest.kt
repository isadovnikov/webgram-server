package com.isadounikau.webgram.infra.controller

import com.isadounikau.webgram.infra.util.PostgresLifecycleManager
import com.isadounikau.webgram.infra.util.WiremockLifecycleManager
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.junit.jupiter.api.Test

@QuarkusTest
@QuarkusTestResource(PostgresLifecycleManager::class)
@QuarkusTestResource(WiremockLifecycleManager::class)
class TelegramResourceIntegrationTest {

    @Test
    fun `should process update message normally`() {
        given()
            .contentType(ContentType.JSON)
            .body(updateMessage)
            .`when`().post("/api/v1/telegrams")
            .then().statusCode(201)
    }

    @Test
    fun `should process new chat members message`() {
        given()
            .contentType(ContentType.JSON)
            .body(newChatMemberMessage)
            .`when`().post("/api/v1/telegrams")
            .then().statusCode(201)
    }

}

val updateMessage = """
    {
      "update_id": 924592143,
      "message": {
        "message_id": 26,
        "from": {
          "id": 183601942,
          "is_bot": false,
          "first_name": "Ih",
          "last_name": "S",
          "username": "isadounikau",
          "language_code": "en"
        },
        "chat": {
          "id": 183601942,
          "first_name": "Ih",
          "last_name": "S",
          "username": "isadounikau",
          "type": "private"
        },
        "date": 1613582250,
        "photo": [
          {
            "file_id": "AgACAgIAAxkBAAMaYC1PqskYTlqXzKLUHPiOPOzbj30AAmmyMRsH1XBJlJRzdPYhlOmozg-bLgADAQADAgADbQADGVcCAAEeBA",
            "file_unique_id": "AQADqM4Pmy4AAxlXAgAB",
            "file_size": 10391,
            "width": 320,
            "height": 320
          },
          {
            "file_id": "AgACAgIAAxkBAAMaYC1PqskYTlqXzKLUHPiOPOzbj30AAmmyMRsH1XBJlJRzdPYhlOmozg-bLgADAQADAgADeAADGlcCAAEeBA",
            "file_unique_id": "AQADqM4Pmy4AAxpXAgAB",
            "file_size": 16131,
            "width": 600,
            "height": 600
          }
        ],
        "caption": "test"
      }
    }

""".trimIndent()

val newChatMemberMessage = """
    {
      "update_id": 924592238,
      "my_chat_member": {
        "chat": {
          "id": -1001374257084,
          "title": "TestIhar222",
          "type": "channel"
        },
        "from": {
          "id": 183601942,
          "is_bot": false,
          "first_name": "Ihar",
          "last_name": "S",
          "username": "isadounikau",
          "language_code": "en"
        },
        "date": 1650480304,
        "old_chat_member": {
          "user": {
            "id": 1623006284,
            "is_bot": true,
            "first_name": "TestIharBot",
            "username": "TestIharBot"
          },
          "status": "kicked",
          "until_date": 0
        },
        "new_chat_member": {
          "user": {
            "id": 1623006284,
            "is_bot": true,
            "first_name": "TestIharBot",
            "username": "TestIharBot"
          },
          "status": "administrator",
          "can_be_edited": false,
          "can_manage_chat": true,
          "can_change_info": false,
          "can_post_messages": true,
          "can_edit_messages": false,
          "can_delete_messages": false,
          "can_invite_users": false,
          "can_restrict_members": true,
          "can_promote_members": false,
          "can_manage_video_chats": false,
          "is_anonymous": false,
          "can_manage_voice_chats": false
        }
      }
    }
""".trimIndent()
