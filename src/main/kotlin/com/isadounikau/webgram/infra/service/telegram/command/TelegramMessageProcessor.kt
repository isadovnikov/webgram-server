package com.isadounikau.webgram.infra.service.telegram.command

import dev.inmo.tgbotapi.types.update.abstracts.Update

abstract class TelegramMessageProcessor<R> {
    protected abstract fun isApplicable(update: Update): Boolean

    suspend fun process(update: Update): Result<R> = kotlin.runCatching {
        if (isApplicable(update)) {
            return execute(update)
        }

        return Result.failure(NotApplicableMessage())
    }

    protected abstract suspend fun execute(update: Update): Result<R>
}

class NotApplicableMessage: Throwable()
