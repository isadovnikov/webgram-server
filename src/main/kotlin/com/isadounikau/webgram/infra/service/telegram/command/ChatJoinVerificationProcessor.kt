package com.isadounikau.webgram.infra.service.telegram.command

import dev.inmo.tgbotapi.bot.TelegramBot
import dev.inmo.tgbotapi.requests.send.SendTextMessage
import dev.inmo.tgbotapi.types.CommonUser
import dev.inmo.tgbotapi.types.message.ChatEvents.NewChatMembers
import dev.inmo.tgbotapi.types.message.CommonSupergroupEventMessage
import dev.inmo.tgbotapi.types.update.MessageUpdate
import dev.inmo.tgbotapi.types.update.abstracts.Update

class ChatJoinVerificationProcessor(
    private val telegramBot: TelegramBot,
) : TelegramMessageProcessor<Unit>() {


    override fun isApplicable(update: Update): Boolean {
        if (update !is MessageUpdate) return false

        val data = update.data
        if (data !is CommonSupergroupEventMessage<*>) return false

        val event = data.chatEvent
        if (event is NewChatMembers) return true

        return false
    }

    override suspend fun execute(update: Update): Result<Unit> = runCatching {
        if (isApplicable(update)) {
            val data = (update as MessageUpdate).data
            val event = (data as CommonSupergroupEventMessage<*>).chatEvent as NewChatMembers
            event.members
                .filterIsInstance<CommonUser>()
                .forEach {
                    val appeal = it.username?.username ?: "${it.firstName} ${it.lastName}"
                    telegramBot.execute(SendTextMessage(data.chat.id, genMessage(appeal)))
                }
        }
    }

    private fun genMessage(appeal: String) = "Добро пожаловать $appeal! Тут принято #intro делать, будь котиком посмотри по хештегу"
}
