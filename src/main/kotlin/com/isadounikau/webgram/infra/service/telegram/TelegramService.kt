package com.isadounikau.webgram.infra.service.telegram

import com.isadounikau.webgram.core.domain.Bot
import com.isadounikau.webgram.infra.config.telegram.TelegramBotProvider
import com.isadounikau.webgram.infra.config.telegram.TelegramConfig
import com.isadounikau.webgram.infra.service.telegram.command.ChatJoinVerificationProcessor
import com.isadounikau.webgram.infra.service.telegram.command.NotApplicableMessage
import dev.inmo.micro_utils.common.bytes
import dev.inmo.tgbotapi.requests.abstracts.MultipartFile
import dev.inmo.tgbotapi.requests.send.SendTextMessage
import dev.inmo.tgbotapi.requests.send.media.SendPhoto
import dev.inmo.tgbotapi.types.toChatId
import dev.inmo.tgbotapi.types.update.abstracts.Update
import dev.inmo.tgbotapi.types.update.abstracts.UpdateDeserializationStrategy
import dev.inmo.tgbotapi.utils.asStorageFile
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import java.io.File

private val log = KotlinLogging.logger { }

class TelegramService(
    private val telegramBotProvider: TelegramBotProvider,
    private val telegramConfig: TelegramConfig,
    private val telegramJson: Json,
) : com.isadounikau.webgram.core.service.TelegramService {

    private val messageProcessors = setOf(
        ChatJoinVerificationProcessor(telegramBotProvider.getBot(Bot.ANATOLY))
    )

    override fun processUpdate(rawUpdate: String): Result<Unit> = runCatching {
        val update = telegramJson.decodeFromString(UpdateDeserializationStrategy, rawUpdate)

        runBlocking {
            messageProcessors.forEach { processor ->
                processor.process(update).onFailure(update)
            }
        }
    }

    private fun Result<Unit>.onFailure(update: Update): Result<Unit> {
        when(val exception = exceptionOrNull()) {
            is NotApplicableMessage -> return this
            else -> log.error(exception) { "Message [$update] processing failed" }
        }
        return this
    }

    override fun sendMessage(bot: Bot, message: String) = runBlocking {
        val channelId = telegramConfig.electricityChannelId().toChatId()
        val t = telegramBotProvider.getBot(bot).execute(SendTextMessage(channelId, message))
    }

    override fun sendImage(bot: Bot, image: File) = runBlocking {
        val channelId = telegramConfig.electricityChannelId().toChatId()
        val inputPhoto = MultipartFile(
            file = image.bytes().asStorageFile("darryl.jpeg")
        )
        val t = telegramBotProvider.getBot(bot).execute(SendPhoto(channelId, inputPhoto))
    }

}


