package com.isadounikau.webgram.infra.service

import com.isadounikau.webgram.core.domain.CheapestRanges
import com.isadounikau.webgram.core.domain.MarketRealTimePrice
import com.isadounikau.webgram.core.domain.TimeTrunc
import com.isadounikau.webgram.core.util.toHourString
import com.isadounikau.webgram.infra.client.MarketRealTimePriceApiResponse
import com.isadounikau.webgram.infra.client.ReeDataApi
import io.github.humbleui.skija.EncodedImageFormat
import io.github.humbleui.skija.Font
import io.github.humbleui.skija.Image
import io.github.humbleui.skija.Paint
import io.github.humbleui.skija.Surface
import io.github.humbleui.skija.TextLine
import io.github.humbleui.skija.Typeface
import io.netty.handler.codec.http.HttpResponseStatus.NOT_FOUND
import io.netty.handler.codec.http.HttpResponseStatus.OK
import mu.KotlinLogging
import org.eclipse.microprofile.rest.client.inject.RestClient
import java.io.File
import java.io.InputStream
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.NotFoundException

private val log = KotlinLogging.logger { }

@ApplicationScoped
class ReeService(
    @RestClient private val api: ReeDataApi
) : com.isadounikau.webgram.core.service.ReeService {

    override fun getMarketRealTimePrice(
        startDate: LocalDateTime,
        endDate: LocalDateTime,
        timeTrunc: TimeTrunc,
    ): Result<MarketRealTimePrice> {
        val response = api.getMarketRealTimePrice(startDate, endDate, timeTrunc.apiValue)

        return when (response.status) {
            OK.code() -> {
                Result.success(response.readEntity(MarketRealTimePriceApiResponse::class.java).toModel())
            }
            NOT_FOUND.code() -> {
                Result.failure(NotFoundException(response.entity.toString()))
            }
            else -> {
                val entity = response.entity.toString()
                log.debug { entity }
                Result.failure(RuntimeException(entity))
            }
        }
    }

    override fun generateElectricityDailyReport(date: LocalDate, ranges: CheapestRanges): Result<File> = kotlin.runCatching {
        val templateImage = this::class.java.getResourceAsStream("/test.png")
            ?.use(InputStream::readBytes)
            ?.let { Image.makeFromEncoded(it) }
            ?: return Result.failure(ClassNotFoundException("Template Image Not Found"))

        val height = templateImage.height - (templateImage.height - 55 * (ranges.ranges.size + 1))
        val surface = Surface.makeRasterN32Premul(templateImage.width, height)
        val canvas = surface.canvas

        canvas.drawImage(templateImage, 0F, 0F)

        val typeface = Typeface.makeDefault()

        val font = Font(typeface, 32F)
        val gray = Paint().apply { color = 0xFFB8B19E.toInt() }
        val green = Paint().apply { color = 0xFF1CA950.toInt() }

        val dateString = date.format(formatter)
        val dateLine = TextLine.make("$dateString PVPC horarios baratos", Font(typeface, 37F))
        canvas.drawTextLine(dateLine, 50F, 50F, gray)

        val rangesTextLines = ranges.ranges.map {
            it.values
        }.map {
            val starts = it.first().toHourString()
            val ends = it.last().let { it.copy(datetime = it.datetime.plusHours(1)) }.toHourString()
            val average = it.map { it.value }.average()
                .toBigDecimal()
                .divide(BigDecimal(1000))
                .setScale(3, RoundingMode.HALF_UP)
            Triple(starts, ends, average)
        }.map { (starts, ends, average) ->
            TextLine.make("$average €/kWh desde $starts hasta $ends", font)
        }

        var step = 100F
        rangesTextLines.forEach {
            canvas.drawTextLine(it, 50F, step, green)
            step += 50F
        }

        val snapshot = surface.makeImageSnapshot()
        val data = snapshot.encodeToData(EncodedImageFormat.JPEG, 95)
            ?: return Result.failure(RuntimeException("Not encoded"))

        val file = File("test.jpg")
        file.writeBytes(data.bytes)

        return Result.success(file)
    }


    private val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
}

private fun MarketRealTimePriceApiResponse.toModel() = MarketRealTimePrice(
    included = this.included.map {
        it.toModel()
    }
)

private fun MarketRealTimePriceApiResponse.Included.toModel() = MarketRealTimePrice.Included(
    id = this.id.toId(),
    type = this.type,
    attributes = this.attributes.toModel()
)

private fun String.toId(): MarketRealTimePrice.Included.ID = MarketRealTimePrice.Included.ID.fromCode(this.toInt())

private fun MarketRealTimePriceApiResponse.Included.Attributes.toModel() = MarketRealTimePrice.Included.Attributes(
    values = this.values.map { it.toModel() }
)

private fun MarketRealTimePriceApiResponse.Included.Attributes.Value.toModel() = MarketRealTimePrice.Included.Attributes.Value(
    value, percentage, datetime
)
