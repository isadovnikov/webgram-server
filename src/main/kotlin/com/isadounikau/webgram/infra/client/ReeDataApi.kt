package com.isadounikau.webgram.infra.client

import com.fasterxml.jackson.annotation.JsonProperty
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import java.time.LocalDateTime
import java.time.OffsetDateTime
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Response

@Path("/es/datos")
@RegisterRestClient(configKey = "ree-data-api")
interface ReeDataApi {

    @GET
    @Path("/mercados/precios-mercados-tiempo-real")
    fun getMarketRealTimePrice(
        @QueryParam("start_date") startDate: LocalDateTime,
        @QueryParam("end_date") endDate: LocalDateTime,
        @QueryParam("time_trunc") timeTrunc: String,
    ): Response
}

data class MarketRealTimePriceApiResponse(
    @JsonProperty("included")
    val included: List<Included>
) {
    data class Included(
        @JsonProperty("id")
        val id: String,
        @JsonProperty("type")
        val type: String,
        @JsonProperty("attributes")
        val attributes: Attributes,
    ) {
        data class Attributes(
            @JsonProperty("values")
            val values: List<Value>
        ) {
            data class Value(
                @JsonProperty("value")
                val value: Double,
                @JsonProperty("percentage")
                val percentage: Double,
                @JsonProperty("datetime")
                val datetime: OffsetDateTime,
            )
        }
    }
}
