package com.isadounikau.webgram.infra.config

import com.isadounikau.webgram.infra.config.telegram.TelegramBotProvider
import com.isadounikau.webgram.infra.config.telegram.TelegramConfig
import com.isadounikau.webgram.infra.service.telegram.TelegramService
import kotlinx.serialization.json.Json
import javax.enterprise.context.Dependent
import javax.enterprise.inject.Produces


@Dependent
class ServiceContextConfig {

    @Produces
    fun telegramService(
        telegramBotProvider: TelegramBotProvider,
        telegramConfig: TelegramConfig,
        telegramJson: Json,
    ) = TelegramService(telegramBotProvider, telegramConfig, telegramJson)

    @Produces
    fun telegramJson() = Json

}
