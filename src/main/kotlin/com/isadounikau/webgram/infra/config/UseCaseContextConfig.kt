package com.isadounikau.webgram.infra.config

import com.isadounikau.webgram.core.service.ReeService
import com.isadounikau.webgram.core.service.TelegramService
import com.isadounikau.webgram.core.usecase.api.ProcessTelegramMessageUseCase
import com.isadounikau.webgram.core.usecase.api.SendTodayElectricityUpdateUseCase
import com.isadounikau.webgram.core.usecase.ree.GetCheapestRangesUseCase
import com.isadounikau.webgram.core.usecase.ree.GetMarketRealTimePriceUseCase
import javax.enterprise.context.Dependent
import javax.enterprise.inject.Produces

@Dependent
class UseCaseContextConfig {

    @Produces
    fun sendElectricityUpdateUseCase(
        telegramService: TelegramService,
        reeService: ReeService,
        getCheapestRangesUseCase: GetCheapestRangesUseCase
    ) = SendTodayElectricityUpdateUseCase(
        telegramService,
        reeService,
        getCheapestRangesUseCase,
    )

    @Produces
    fun getCheapestRangesUseCase(
        getMarketRealTimePriceUseCase: GetMarketRealTimePriceUseCase
    ) = GetCheapestRangesUseCase(getMarketRealTimePriceUseCase)

    @Produces
    fun getMarketRealTimePriceUseCase(
        reeService: ReeService,
    ) = GetMarketRealTimePriceUseCase(
        reeService,
    )

    @Produces
    fun processTelegramMessageUseCase(
        telegramService: TelegramService
    ) = ProcessTelegramMessageUseCase(telegramService)

}
