package com.isadounikau.webgram.infra.config.telegram

import com.isadounikau.webgram.core.domain.Bot
import dev.inmo.tgbotapi.bot.Ktor.telegramBot
import dev.inmo.tgbotapi.bot.TelegramBot
import dev.inmo.tgbotapi.utils.TelegramAPIUrlsKeeper

class TelegramBotProvider(
    config: TelegramConfig
) {

    private val anatolyGatekeeperBot = TelegramAPIUrlsKeeper(
        token = config.anatoly(),
        hostUrl = config.host(),
    ).let { telegramBot(it) }

    private val electricityBot = TelegramAPIUrlsKeeper(
        token = config.electricity(),
        hostUrl = config.host(),
    ).let { telegramBot(it) }

    fun getBot(bot: Bot): TelegramBot = when (bot) {
        Bot.ANATOLY -> anatolyGatekeeperBot
        Bot.ELECTRICITY -> electricityBot
    }
}
