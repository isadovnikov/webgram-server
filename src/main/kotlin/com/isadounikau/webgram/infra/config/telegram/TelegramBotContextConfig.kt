package com.isadounikau.webgram.infra.config.telegram

import io.smallrye.config.ConfigMapping
import javax.enterprise.inject.Produces

class TelegramBotContextConfig {

    @Produces
    fun telegramBotProvider(
        config: TelegramConfig
    ) = TelegramBotProvider(config)
}

@ConfigMapping(prefix = "telegram.bot")
interface TelegramConfig {
    fun host(): String
    fun anatoly(): String
    fun electricity(): String
    fun electricityChannelId(): Long
}
