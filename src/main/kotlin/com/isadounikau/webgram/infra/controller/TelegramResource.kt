package com.isadounikau.webgram.infra.controller

import com.isadounikau.webgram.core.usecase.api.ProcessTelegramMessageUseCase
import mu.KotlinLogging
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.ext.Provider

private val log = KotlinLogging.logger { }

@Provider
@Path("/api/v1/telegrams")
class TelegramResource(
    private val processTelegramMessageUseCase: ProcessTelegramMessageUseCase,
) {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    fun createTelegramMessage(requestBody: String): Response {
        processTelegramMessageUseCase.invoke(requestBody)
        return Response.status(Response.Status.CREATED).build()
    }
}
