package com.isadounikau.webgram.infra.controller

import com.isadounikau.webgram.core.usecase.ree.GetCheapestRangesUseCase
import com.isadounikau.webgram.core.usecase.ree.GetCheapestRangesUseCase.GetCheapestResponse.Failed.Error
import com.isadounikau.webgram.core.usecase.ree.GetCheapestRangesUseCase.GetCheapestResponse.Success
import com.isadounikau.webgram.core.util.nowInSpain
import mu.KotlinLogging
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.core.Response
import javax.ws.rs.ext.Provider

private val log = KotlinLogging.logger { }

@Provider
@Path("/api/v1/ree")
class ReeResource(
    private val getCheapestRangesUseCase: GetCheapestRangesUseCase
) {

    @GET
    @Path("/tomorrow-cheapest")
    fun getTomorrowCheapest(): Response {
        val date = nowInSpain()
        return when (val response = getCheapestRangesUseCase.invoke(GetCheapestRangesUseCase.GetCheapestRequest(date))) {
            is Success -> Response.ok().entity(response.data).build()
            is Error -> Response.serverError().entity(response.message).build().also {
                log.error { response.message }
            }
            is GetCheapestRangesUseCase.GetCheapestResponse.Failed.NotFound -> Response.noContent().entity(response.message).build().also {
                log.warn { response.message }
            }
        }
    }
}
