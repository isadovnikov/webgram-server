package com.isadounikau.webgram.infra.controller

import com.isadounikau.webgram.core.usecase.api.SendTodayElectricityUpdateUseCase
import com.isadounikau.webgram.core.usecase.api.SendTodayElectricityUpdateUseCase.SendElectricityUpdateRequest
import com.isadounikau.webgram.core.usecase.api.SendTodayElectricityUpdateUseCase.SendElectricityUpdateResponse.Failed.Error
import com.isadounikau.webgram.core.usecase.api.SendTodayElectricityUpdateUseCase.SendElectricityUpdateResponse.Success
import mu.KotlinLogging
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.core.Response
import javax.ws.rs.ext.Provider

private val log = KotlinLogging.logger { }

@Provider
@Path("/api/v1/commands")
class CommandTriggerResource(
    private val sendTodayElectricityUpdateUseCase: SendTodayElectricityUpdateUseCase
) {

    @GET
    @Path("/electricity-update")
    fun triggerElectricityUpdate(): Response {
        return when (val response = sendTodayElectricityUpdateUseCase.invoke(SendElectricityUpdateRequest)) {
            is Success -> Response.ok().build()
            is Error -> Response.serverError().entity(response.message).build().also {
                log.error { response.message }
            }
        }
    }
}
